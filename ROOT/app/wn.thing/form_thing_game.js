([{
    key :"id",
    title :"ID",
    hide :true,
    type :"string",
    editAs :"label"
}, {
    key :"title",
    title :"游戏名称",
    type :"string",
    editAs :"input"
}, {
    key :"developer",
    title :"开发者",
    type :"string",
    editAs :"text"
}, {
    key :"gameurl",
    title :"游戏地址",
    type :"string",
    editAs :"text"
}, {
    key :"lm",
    title :"最后上传时间",
    type :"datetime",
    editAs :"label"
}])