define({
    "thing" : {
        "fld" : {
            "general" : "基本信息",
            "numerical" : "统计信息"
        },
        "key" : {
            "id" : "ID",
            "lbls" : "标签",
            "ct" : "创建时间",
            "lm" : "最后修改",
            "icon" : "图标",
            "th_ow" : "所有者",
            "th_nm" : "名称",
            "th_brief" : "摘要",
            "th_live" : "存活",
            "th_cate" : "分类",
            "th_c_cmt" : "评论数",
            "th_c_view" : "浏览数",
            "th_c_agree" : "赞同数",
        },
        "live_a" : "有效",
        "live_d" : "已删除",
        "err" : {
            "nothingjs" : "没有找到 thing.js!"
        },
        "blank" : '请从左侧列表中选择一个项目查看详情和编辑'
    }
});