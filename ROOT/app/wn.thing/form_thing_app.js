([{
    key :"id",
    title :"ID",
    hide :true,
    type :"string",
    editAs :"label"
}, {
    key :"title",
    title :"应用名称",
    type :"string",
    editAs :"input"
}, {
    key :"alias",
    title :"应用概要",
    type :"string",
    editAs :"text"
}, {
    key :"downurl",
    title :"下载地址",
    type :"string",
    editAs :"text"
}, {
    key :"lm",
    title :"最后上传时间",
    type :"datetime",
    editAs :"label"
}])