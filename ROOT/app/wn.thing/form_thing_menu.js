([{
    key :"id",
    title :"ID",
    hide :true,
    type :"string",
    editAs :"label"
}, {
    key :"the_name",
    title :"名称",
    type :"string",
    editAs :"input"
}, {
    key :"breief",
    title :"描述",
    type :"string",
    editAs :"input"
}, {
    key :"price",
    title :"价格",
    type :"int",
    editAs :"input",
    uiWidth : 100,
    uiConf : {
        unit : "RMB"
    }
}, {
    key :"price2",
    title :"会员",
    type :"int",
    editAs :"input",
    uiWidth : 100,
    uiConf : {
        unit : "RMB"
    }
}, {
    key :"lm",
    title :"最后修改时间",
    type :"datetime",
    editAs :"label"
}])