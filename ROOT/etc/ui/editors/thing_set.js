({
	key : "thing_set",
	text : "ThingSetEditor",
	icon : '<i class="fa fa-cube"></i>',
	outline : false,
	actions : [ "@::save_text","@::upload" ],
	uiType : "app/wn.thing/thing",
	uiConf : {}
})